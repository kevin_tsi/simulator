from setuptools import setup, find_packages


def read_requirements(filename):
    return [
        req.strip() for req in open(filename)
        if not req.startswith('-r') and not req.startswith('#')
    ]


requirements = read_requirements('requirements.txt')
dev_requirements = read_requirements('requirements-tests.txt')


setup(
    name='wattsim-simulator',
    version=open('VERSION').read().strip(),
    author='Erwan Rouchet',
    author_email='lucidiot@protonmail.com',
    license='GNU General Public License v3 (GPLv3)',
    packages=find_packages(
        exclude=["*.tests", "*.tests.*", "tests.*", "tests"],
    ),
    package_data={
        '': ['*.md', 'LICENSE', 'README'],
    },
    python_requires='>=3.5',
    install_requires=requirements,
    extras_require={
        'dev': dev_requirements,
    },
    tests_require=dev_requirements,
    description='Station simulator for the WattSim project',
    long_description=open('README.rst').read(),
    long_description_content_type='text/x-rst',
    keywords='django arduino energy',
    url='https://gitlab.com/wattsim/simulator',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Environment :: No Input/Output (Daemon)',
        'Intended Audience :: Education',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English'
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Education',
        'Topic :: Utilities',
    ],
)
